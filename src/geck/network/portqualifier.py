#!/bin/python
from PySide2.QtCore import QObject, Property, Signal
from typing import Any

class PortQualifier(QObject):
    def __init__(self, node_id, port_id, parent=None) -> None:
        QObject.__init__(self, parent)
        self.node_id_ = node_id
        self.port_id_ = port_id
    
    def get_node_id(self) -> Any:
        return self.node_id_
    
    def get_port_id(self) -> Any:
        return self.port_id_
    
    def __repr__(self):
        return 'PortQualifier:{}:{}'.format(self.get_node_id(),self.get_port_id())
    
    node_id_changed = Signal()
    port_id_changed = Signal()
    node_id = Property(int, get_node_id, notify=node_id_changed)
    port_id = Property(int, get_port_id, notify=port_id_changed)