from PySide2.QtCore import Slot, Property, Signal, QObject
from PySide2.QtGui import QImage
from geck.network import Node, Network, Parameter
from typing import Union, Optional, Callable, List, TYPE_CHECKING
from copy import deepcopy

if TYPE_CHECKING:
    from .controller import Controller

class NodeGenerator(QObject):
    name_changed_signal = Signal()
    icon_changed_signal = Signal()
    initial_parameter_query_changed_signal = Signal()
    parameters_changed = Signal()

    def __init__(self, 
                 node_class,
                 parent : QObject = None) -> None:
        QObject.__init__(self, parent = parent)
        self.node_class_ = node_class
        self.name_ = node_class.node_name()
        self.parameters_ = node_class.default_parameters()
        self.initial_parameter_query_ = True
        self.icon_ = None
        self.controller_ = None
        pass

    def set_controller(self, controller : 'Controller'):
        self.controller_ = controller

    @Slot(str, result=Node)
    def generate(self, name) -> Optional[Node]:
        parameter_copies = [deepcopy(p) for p in self.parameters_]
        new_node = self.node_class_(network=self.controller_.get_network(), name=name, parameters=parameter_copies)
        self.controller_.get_network().add_node(new_node)
        return new_node
    
    # icon functions
    def get_icon(self) -> Optional[QImage]:
        return self.icon_

    def set_icon(self, icon : Union[QImage,str,None]) -> None:
        if icon != self.icon_:
            # load from file, if is string
            if type(icon) == str:
                icon_str = icon
                icon = QImage()
                icon.load(icon_str)
            # set icon
            self.icon_ = icon
            self.icon_changed_signal.emit()

    # name functions
    def get_name(self) -> str:
        return self.name_

    def set_name(self, name) -> None:
        if name != self.name_:
            self.name_ = name
            self.name_changed_signal.emit()
    
    # initial parameter query functions

    def get_intial_parameter_query(self) -> bool:
        return self.initial_parameter_query_

    def set_initial_parameter_query(self, initial_parameter_query):
        if initial_parameter_query != self.initial_parameter_query_:
            self.initial_parameter_query_ = initial_parameter_query
            initial_parameter_query_changed_signal.emit()
    
    # parameters functions

    def get_parameters(self) -> List[Parameter]:
        return self.parameters_

    def set_parameters(self, parameters) -> None:
        if self.parameters_ != parameters:
            self.parameters_ = parameters
            self.parameters_changed.emit()
    
    # properties
    name = Property(str, get_name, set_name, notify=name_changed_signal)
    icon = Property(QImage, get_icon, set_icon, notify=icon_changed_signal)
    initial_parameter_query = Property(bool, get_intial_parameter_query, set_initial_parameter_query, notify=initial_parameter_query_changed_signal)
    parameters    = Property('QVariantList', get_parameters, notify=parameters_changed)
