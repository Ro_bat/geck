from PySide2.QtCore import QObject, Property, Signal
from typing import Any
from copy import deepcopy

class PortDataType(QObject):
    data_type_changed           = Signal()
    data_type_string_changed    = Signal()
    details_changed      = Signal()

    def data_type_equal(port_data_a, port_data_b):
        return port_data_a.get_data_type()==port_data_b.get_data_type()

    def data_type_and_details_equal(port_data_a, port_data_b):
        return PortDataType.data_type_equal(port_data_a, port_data_b) and (port_data_a.get_details()==port_data_b.get_details())

    def data_type_and_details_equal_allow_none(port_data_a, port_data_b):
        data_type_eq = PortDataType.data_type_equal(port_data_a, port_data_b)
        details_equal = port_data_a.get_details()==port_data_b.get_details()
        either_detail_none = (port_data_a.get_details()==None) or (port_data_b.get_details()==None)
        return data_type_eq and (details_equal or either_detail_none)

    def __init__(self,
                 data_type : str = 'unspecified',
                 data_type_string : str = 'Unspecified',
                 details : str = '',
                 connection_check_function = data_type_and_details_equal_allow_none,
                 parent : QObject = None) -> None:
        QObject.__init__(self, parent=parent)
        self.data_type_        = data_type
        self.data_type_string_ = data_type_string
        self.details_          = details
        self.connection_check_function_ = connection_check_function

    # data type functions
    def get_data_type(self):
        return self.data_type_

    def set_data_type(self, data_type):
        if data_type != self.data_type_:
            self.data_type_ = data_type
            self.data_type_changed.emit()

    # data type string functions
    def get_data_type_string(self):
        return self.data_type_string_

    def set_data_type_string(self, data_type_string):
        if data_type_string != self.data_type_string_:
            self.data_type_string_ = data_type_string
            self.data_type_string_changed.emit()

    # data details string functions
    def get_details(self) -> str:
        return self.details_

    def set_details(self, details : str):
        if details != self.details_:
            self.details_ = details
            self.details_changed.emit()
    
    def can_connect(self, other):
        return self.connection_check_function_(self,other)

    # copy
    def __deepcopy__(self, memo):
        copy = type(self)(data_type = deepcopy(self.get_data_type()),
                          data_type_string = deepcopy(self.get_data_type_string()),
                          details = deepcopy(self.get_details()),
                          connection_check_function = self.connection_check_function_,
                          parent = None)
        return copy
    
    def __repr__(self):
        return 'PortDataType:{}:{}'.format(self.get_data_type_string(),self.get_details_string())
    
    data_type        = Property(str, get_data_type       , notify=data_type_changed)
    data_type_string = Property(str, get_data_type_string, notify=data_type_string_changed)
    details   = Property(str, get_details, set_details, notify=details_changed)
