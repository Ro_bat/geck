#!/bin/python

from .node import Node
from .port import Port, PortType
from .portqualifier import PortQualifier
from .connection import Connection
from PySide2.QtCore import Signal, Property
from typing import Any, List

class Network(Node):
    connections_changed = Signal()
    nodes_changed       = Signal()
    network_inports_changed  = Signal()
    network_outports_changed = Signal()

    def __init__(self, name, parameters=[], detailed_name : str=None, controller_data=None, parent = None) -> None:
        Node.__init__(self, None, name, parameters=parameters, detailed_name=detailed_name, parent = parent)
        self.node_id_ctr_ = 1
        self.connections_ = []
        self.nodes_ = []
        self.network_inports_  = []
        self.network_outports_ = []
        self.controller_data_ = controller_data

    def get_port_by_qualifier(self, port_qualifier : PortQualifier) -> Port:
        node = self.get_node_by_node_id(port_qualifier.get_node_id())
        if node:
            return node.get_port_by_port_id(port_qualifier.get_port_id())
        else:
            return None
    
    def get_node_by_node_id(self, node_id : Any) -> Node:
        if node_id == None:
            return self
        for node in self.nodes_:
            if node.get_node_id() == node_id:
                return node
        return None
    
    def get_node_by_node_name(self, node_name : str) -> Node:
        for node in self.nodes_:
            if node.get_name() == node_name:
                return node
        return None

    def get_controller_data(self) -> Any:
        return self.controller_data_
        
    def get_connections(self) -> List[Connection]:
        return self.connections_
        
    def get_nodes(self) -> List[Node]:
        return self.nodes_
    
    def add_node(self, node : Node) -> None:
        self.nodes_.append(node)
        self.nodes_changed.emit()
    
    def remove_node(self, node : Node) -> None:
        node_id = node.get_node_id()
        affected_connections = [c for c in self.connections_ if (c.get_srcport().get_node_id()==node_id or c.get_dstport().get_node_id()==node_id)]
        connected_node_ids  = {c.get_srcport().get_node_id() for c in affected_connections}
        connected_node_ids.update({c.get_dstport().get_node_id() for c in affected_connections})
        connected_nodes =  [n for n in self.nodes_ if n.get_node_id() in connected_node_ids and n.get_node_id()!=node.get_node_id()]
        for connection in affected_connections:
            connection.delete_callback()
        if len(affected_connections)>0:
            self.connections_ = [c for c in self.connections_ if c not in affected_connections]
            self.connections_changed.emit()
        node.delete_callback()
        self.nodes_.remove(node)
        self.nodes_changed.emit()
        for connected_node in connected_nodes:
            connected_node.connections_update()
    
    def geck_connect(self, connection : Connection) -> None:
        if not self.has_connection(connection):
            self.connections_.append(connection)
            self.connections_changed.emit()
            self.get_node_by_node_id(connection.get_srcport().get_node_id()).connections_update()
            self.get_node_by_node_id(connection.get_dstport().get_node_id()).connections_update()
    
    def geck_disconnect(self, connection : Connection) -> None:
        connections_filtered = [c for c in self.connections_ if c.get_dstport()==connection.get_dstport() and c.get_srcport()==connection.get_srcport()]
        if len(connections_filtered)>0:
            self.connections_ = [c for c in self.connections_ if c not in connections_filtered]
            self.connections_changed.emit()
            self.get_node_by_node_id(connection.get_srcport().get_node_id()).connections_update()
            self.get_node_by_node_id(connection.get_dstport().get_node_id()).connections_update()
    
    def has_connection(self, connection : Connection) -> bool:
        connections_filtered = [c for c in self.connections_ if c.get_dstport()==connection.get_dstport() and c.get_srcport()==connection.get_srcport()]
        return len(connections_filtered)>0
    
    def request_node_id(self) -> Any:
        ret = self.node_id_ctr_
        self.node_id_ctr_ += 1
        return ret
    
    def get_node_connections_inbound(self, node_id):
        return [connection for connection in self.connections_ if connection.get_dstport().get_node_id() == node_id]
    
    def get_node_connections_outbound(self, node_id):
        return [connection for connection in self.connections_ if connection.get_srcport().get_node_id() == node_id]
    
    def get_network_inports(self) -> List[Port]:
        return self.inports_

    def get_network_outports(self) -> List[Port]:
        return self.outports_
    
    def add_inport(self, port : str, controller_data : Any = None) -> Port:
        new_port = Port(self, port, PortType.INPORT, controller_data=controller_data)
        new_network_port = Port(self, port, PortType.OUTPORT, controller_data=controller_data)
        self.inports_.append(new_port)
        self.inports_changed.emit()
        self.network_inports_.append(new_network_port)
        self.network_inports_changed.emit()
        return new_port
    
    def add_outport(self, port : str, controller_data : Any = None):
        new_port = Port(self, port, PortType.OUTPORT, controller_data=controller_data)
        new_network_port = Port(self, port, PortType.INPORT, controller_data=controller_data)
        self.outports_.append(new_port)
        self.outports_changed.emit()
        self.network_outports_.append(new_network_port)
        self.network_outports_changed.emit()
        return new_port
    
    nodes       = Property('QVariantList', get_nodes, notify=nodes_changed)
    connections = Property('QVariantList', get_connections, notify=connections_changed)
    network_inports  = Property('QVariantList', get_network_inports , notify=network_inports_changed)
    network_outports = Property('QVariantList', get_network_outports, notify=network_outports_changed)
    

