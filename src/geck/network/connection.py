#!/bin/python
from PySide2.QtCore import QObject, Property, Signal
from .portqualifier import PortQualifier
from typing import Any

class Connection(QObject):
    def __init__(self, srcport, dstport, deletable = True, controller_data=None, parent = None) -> None:
        QObject.__init__(self, parent)
        self.srcport_ = srcport
        self.dstport_ = dstport
        self.deletable_ = deletable
        self.controller_data_ = controller_data

    def get_controller_data(self) -> Any:
        return self.controller_data_

    def get_srcport(self) -> PortQualifier:
        return self.srcport_

    def get_dstport(self) -> PortQualifier:
        return self.dstport_

    def get_deletable(self) -> bool:
        return self.deletable_

    def set_srcport(self, port : PortQualifier) -> None:
        if port != self.srcport_:
            self.srcport_ = port
            self.srcport_changed.emit()

    def set_dstport(self, port : PortQualifier) -> None:
        if port != self._dstport:
            self.dstport_ = port
            self.dstport_changed.emit()
    
    def delete_callback(self) -> None:
        return
    
    def __repr__(self):
        return "{}:{}=>{}:{}".format(self.srcport_.get_node_id(),self.srcport_.get_port_id(),self.dstport_.get_node_id(),self.dstport_.get_port_id())
    
    srcport_changed = Signal()
    dstport_changed = Signal()
    deletable_changed = Signal()
    srcport   = Property(PortQualifier, get_srcport, notify=srcport_changed)
    dstport   = Property(PortQualifier, get_dstport, notify=dstport_changed)
    deletable = Property(bool, get_deletable, notify=deletable_changed)