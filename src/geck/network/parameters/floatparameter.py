from geck.network.parameter import Parameter
from PySide2.QtCore import QObject, Signal, Property, Slot
from typing import Any
import sys
import math
from copy import deepcopy

class FloatParameter(Parameter):
    min_value_changed = Signal()
    max_value_changed = Signal()
    def __init__(self, 
                 name: str,
                 id: str,
                 description: str = '',
                 editable : bool = True,
                 default_value = None,
                 min_value : float = -(2**31-1),
                 max_value : float = 2**31-1,
                 input_hint : str = '',
                 parent: QObject = None) -> None:
        Parameter.__init__(self,
                           name,
                           id,
                           description=description,
                           editable=editable,
                           default_value=default_value,
                           input_hint=input_hint,
                           parameter_type='base:float')
        self.min_value_ = min_value
        self.max_value_ = max_value
        if self.get_value() == None:
            self.set_value(self.min_value_)
        self.validate_value()
    
    # min_value functions
    def get_min_value(self) -> float:
        return self.min_value_

    def set_min_value(self, min_value):
        if self.min_value_ != min_value:
            self.min_value_ = min_value
            self.min_value_changed.emit()
    
    # max_value functions
    def get_max_value(self) -> float:
        return self.max_value_

    def set_max_value(self, max_value):
        if self.max_value_ != max_value:
            self.max_value_ = max_value
            self.max_value_changed.emit()

    # parameter validation
    def validate_value(self) -> bool:
        # check if int
        if math.isnan(self.get_temp_value()) or math.isinf(self.get_temp_value()):
            self.set_value_valid(False)
            return self.get_value_valid()
        # check if value>min
        if (self.min_value_ != None) and (self.get_temp_value() < self.min_value_ ):
            self.set_parameter_error_text('Value has to be greater or equal to {}. (current: {})'.format(self.min_value_, self.get_temp_value()))
            self.set_value_valid(False)
            return self.get_value_valid()
        # check if value<max
        if (self.max_value_ != None) and (self.get_temp_value() > self.max_value_ ):
            self.set_parameter_error_text('Value has to be lesser or equal to {}. (current: {})'.format(self.max_value_, self.get_temp_value()))
            self.set_value_valid(False)
            return self.get_value_valid()
        self.set_value_valid(True)
        return self.get_value_valid()
    
    # cast value
    def cast_value(self, value) -> None:
        return int(value)

    # copy
    def __deepcopy__(self, memo):
        copy = type(self)(name = deepcopy(self.get_name()),
                          id = deepcopy(self.get_id()),
                          description = deepcopy(self.get_description()),
                          editable=deepcopy(self.get_editable()),
                          default_value = deepcopy(self.default_value_),
                          min_value = deepcopy(self.get_min_value()),
                          max_value = deepcopy(self.get_max_value()),
                          input_hint = deepcopy(self.get_input_hint()),
                          parent = None)
        copy.set_value(self.get_value())
        return copy

    #properties
    min_value = Property(float, get_min_value, set_min_value, notify=min_value_changed)
    max_value = Property(float, get_max_value, set_max_value, notify=max_value_changed)