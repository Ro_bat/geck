from __future__ import annotations
from PySide2.QtCore import QObject, Signal, Property, Slot
from enum import IntEnum
from typing import Any
import copy

class Parameter(QObject):
    name_changed = Signal()
    description_changed = Signal()
    editable_changed = Signal()
    value_changed = Signal()
    temp_value_changed = Signal()
    parameter_type_changed = Signal()
    input_hint_changed = Signal()
    parameter_error_text_changed = Signal()
    value_valid_changed = Signal()

    def __init__(self,
                 name : str,
                 id : str,
                 default_value : Any = None,
                 description : str = '',
                 editable : bool = True,
                 parameter_type : str = 'base:invalid',
                 input_hint : str = '',
                 parent: QObject = None) -> None:
        QObject.__init__(self, parent = parent)
        self.name_ = name
        self.id_ = id
        self.description_ = description
        self.value_ = default_value
        self.editable_ = editable
        self.temp_value_ = default_value
        self.default_value_ = default_value
        self.value_valid_ = False
        self.parameter_error_text_ = None
        self.parameter_type_ = parameter_type
        self.input_hint_ = input_hint

    # name functions
    def get_name(self):
        return self.name_
    
    def set_name(self, name):
        if self.name_ != name:
            self.name_ = name
            self.name_changed.emit()
    
    # id functions

    def get_id(self):
        return self.id_

    def set_id(self, id):
        self.id_ = id

    # description functions
    def get_description(self):
        return self.description_
    
    def set_description(self, description):
        if self.description_ != description:
            self.description_ = description
            self.description_.emit()
    
    # editable functions
    def get_editable(self):
        return self.editable_
    
    def set_editable(self, editable):
        if self.editable_ != editable:
            self.editable_ = editable
            self.editable_changed.emit()

    # parameter type functions
    def get_parameter_type(self) -> str:
        return self.parameter_type_
    
    def set_parameter_type(self, parameter_type : str) -> None:
        if int(self.parameter_type_) != parameter_type:
            self.parameter_type_ = parameter_type
            self.parameter_type_changed.emit()
    
    # parameter error text functions
    def get_parameter_error_text(self) -> str:
        return self.parameter_error_text_

    def set_parameter_error_text(self, text : str) -> None:
        if self.parameter_error_text_ != text:
            self.parameter_error_text_ = text
            self.parameter_error_text_changed.emit()
    
    # value functions
    def get_value(self) -> Any:
        return self.value_

    def set_value(self, value : Any) -> None:
        casted_value = self.cast_value(value)
        if self.value_ != casted_value:
            self.value_ = casted_value
            self.value_changed.emit()
            self.set_temp_value(value)
    
    # value functions
    def get_temp_value(self) -> Any:
        return self.temp_value_

    def set_temp_value(self, value : Any) -> None:
        casted_value = self.cast_value(value)
        if self.temp_value_ != casted_value:
            self.temp_value_ = casted_value
            self.temp_value_changed.emit()
            self.validate_value()

    # value_valid functions

    def get_value_valid(self) -> bool:
        return self.value_valid_

    def set_value_valid(self, value_valid : bool):
        if self.value_valid_ != value_valid:
            self.value_valid_ = value_valid
            self.value_valid_changed.emit()

    # cast value
    def cast_value(self, value) -> None:
        return value
    
    # input hint functions

    def get_input_hint(self) -> str:
        return self.input_hint_

    def set_input_hint(self, input_hint) -> None:
        if self.input_hint_ != input_hint:
            self.input_hint = input_hint
            self.input_hint_changed.emit()

    # parameter validation function. Should be overwritten in subclasses
    def validate_value(self) -> bool:
        self.set_parameter_error_text('The Parameter Base class should not be used directly.')
        self.set_value_valid(False)
        return self.get_value_valid()
    
    # confirm/undo changes
    @Slot()
    def undo_changes(self) -> None:
        self.temp_value_ = self.value_
        self.value_changed.emit()

    @Slot()
    def confirm_changes(self) -> None:
        self.value_ = self.temp_value_
        self.value_changed.emit()
    
    @Slot()
    def reset_default(self) -> None:
        self.set_value(self.default_value_)
        self.set_temp_value(self.default_value_)

    # for debugging
    def __repr__(self):
        return 'Parameter:"{}":{}'.format(self.get_name(),self.get_value())

    # properties
    name = Property(str, get_name, set_name, notify=name_changed)
    description = Property(str, get_description, set_description, notify=description_changed)
    editable = Property(bool, get_editable, set_editable, notify=editable_changed)
    parameter_type = Property(str, get_parameter_type, set_parameter_type, notify=parameter_type_changed)
    parameter_error_text = Property(str, get_parameter_error_text, set_parameter_error_text, notify=parameter_error_text_changed)
    value_valid = Property(bool, get_value_valid, notify=value_valid_changed)
    input_hint = Property(str, get_input_hint, set_input_hint, notify=input_hint_changed)

    # value type changes with each subclass. not sure how to handle this yet.
    value = Property('QVariant', get_value, set_value, notify=value_changed)
    temp_value = Property('QVariant', get_temp_value, set_temp_value, notify=temp_value_changed)
