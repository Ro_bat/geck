#!/bin/python

from enum import Enum
from .portqualifier import PortQualifier
from .portdatatype import PortDataType
from .connection import Connection
from PySide2.QtCore import QObject, Property, Signal, Slot
from typing import Any, List, TYPE_CHECKING

if TYPE_CHECKING:
    from .node import Node

class PortType(Enum):
    INVALID = 0
    INPORT = 1
    OUTPORT = 2
    INOUTPORT = 3

class Port(QObject):
    name_changed           = Signal()
    port_id_changed        = Signal()
    port_type_changed      = Signal()
    port_data_type_changed = Signal()
    qualifier_changed      = Signal()
    def __init__(self, node : 'Node', name : str, port_type : PortType = PortType.INVALID, port_data_type : PortDataType = PortDataType(), controller_data : Any = None) -> None:
        QObject.__init__(self)
        self.node_      = node
        self.name_      = name
        self.port_type_ = port_type
        self.port_data_type_ = port_data_type
        self.port_id_   = node.request_port_id()
        self.controller_data_ = controller_data
        self.qualifier_ = PortQualifier(self.node_.get_node_id(), self.get_port_id())

    def get_controller_data(self) -> Any:
        return self.controller_data_

    def get_qualifier(self) -> PortQualifier:
        return self.qualifier_
    
    def get_name(self) -> str:
        return self.name_
    
    def set_name(self, new_name: str) -> None:
        if self.name_ != new_name:
            self.name_ = new_name
            self.name_changed.emit()
    
    def get_port_id(self) -> Any:
        return self.port_id_
    
    def get_port_type(self) -> str:
        return str(self.port_type_)
    
    def get_port_type_raw(self) -> PortType:
        return self.port_type_
    
    def get_port_data_type(self) -> PortDataType:
        return self.port_data_type_
    
    def set_port_data_type(self, port_data_type) -> None:
        if self.port_data_type_ != port_data_type:
            self.port_data_type_ = port_data_type
            self.port_data_type_changed.emit()
    
    def get_connections(self) -> List[Connection]:
        node = self.node_
        all_connections = node.get_network().get_connections() if node.get_network() else node.get_connections()
        relevant_connections = [c for c in all_connections if c.get_srcport()==self.get_qualifier() or c.get_dstport()==self.get_qualifier()]
        return relevant_connections
    
    @Slot('QVariant', result=bool)
    def can_connect(self, other_port: 'QVariant') -> bool:
        typeA = self.get_port_type_raw()
        typeB = other_port.get_port_type_raw()
        check1 = (typeA==PortType.INPORT and typeB==PortType.OUTPORT)
        check2 = (typeB==PortType.INPORT and typeA==PortType.OUTPORT)
        if not check1 and not check2:
            return False
        else:
            return self.get_port_data_type().can_connect(other_port.get_port_data_type())
    
    qualifier      = Property(PortQualifier, get_qualifier, notify=qualifier_changed)
    name           = Property(str, get_name, set_name, notify=name_changed)
    port_id        = Property(int, get_port_id, notify=port_id_changed)
    port_type      = Property(str, get_port_type, notify=port_type_changed)
    port_data_type = Property(PortDataType, get_port_data_type, set_port_data_type, notify=port_data_type_changed)
