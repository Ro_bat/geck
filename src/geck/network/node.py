#!/bin/python

from .port import Port, PortType
from .parameter import Parameter
from PySide2.QtCore import QObject, Signal, Property, Slot
from typing import Any, Optional, List
from copy import copy

class Node(QObject):
    name_changed          = Signal()
    detailed_name_changed = Signal()
    node_id_changed       = Signal()
    inports_changed       = Signal()
    outports_changed      = Signal()
    parameters_changed    = Signal()
    deletable_changed     = Signal()

    # Node Name used for node generator
    def node_name():
        return 'Node'

    def __init__(self, network, name, parameters : List[Parameter] = [], detailed_name = None, deletable = False, controller_data = None, parent = None) -> None:
        QObject.__init__(self, parent = parent)
        self.network_  = network
        self.name_     = name
        self.node_id_  = self.init_id()
        self.inports_  = []
        self.outports_ = []
        self.parameters_ = parameters
        self.detailed_name_ = detailed_name
        self.deletable_ = deletable
        self.port_id_ctr_ = 0
        self.controller_data_ = controller_data
    
    def finalize_generation(self):
        pass

    def get_controller_data(self) -> Any:
        return self.controller_data_

    def get_deletable(self) -> bool:
        return self.deletable_

    def init_id(self) -> Any:
        if self.network_ is None:
            return None
        else:
            return self.network_.request_node_id()
    
    def add_inport(self, port : str, controller_data : Any = None) -> Port:
        new_port = Port(self, port, PortType.INPORT, controller_data=controller_data)
        self.inports_.append(new_port)
        self.inports_changed.emit()
        return new_port
    
    def add_outport(self, port : str, controller_data : Any = None):
        new_port = Port(self, port, PortType.OUTPORT, controller_data=controller_data)
        self.outports_.append(new_port)
        self.outports_changed.emit()
        return new_port
    
    def remove_port(self, port_id) -> None:
        removed_port = self.get_port_by_port_id(port_id)
        if removed_port:
            self.inports_ = [p for p in self.inports_ if p.get_port_id()!=port_id]
            self.inports_changed.emit()
            self.outports_ = [p for p in self.outports_ if p.get_port_id()!=port_id]
            self.outports_changed.emit()
            for connection in removed_port.get_connections():
                self.get_network().geck_disconnect(connection)
            
    
    def get_port_by_name(self, name : str):
        for port in self.inports_:
            if port.name_ == name:
                return port
        for port in self.outports_:
            if port.name_ == name:
                return port
        return None
    
    def get_port_by_port_id(self, port_id : Any) -> Optional[Port]:
        for port in self.inports_:
            if port.get_port_id() == port_id:
                return port
        for port in self.outports_:
            if port.get_port_id() == port_id:
                return port
        return None

    def get_name(self) -> str:
        return self.name_

    def get_detailed_name(self) -> str:
        return self.detailed_name_

    def get_inports(self) -> List[Port]:
        return self.inports_

    def get_outports(self) -> List[Port]:
        return self.outports_

    def get_ports(self) -> List[Port]:
        ports = copy(self.inports_)
        ports.extend(self.outports_)
        return ports

    def get_node_id(self) -> Any:
        return self.node_id_
    
    def request_port_id(self) -> int:
        ret = self.port_id_ctr_
        self.port_id_ctr_ += 1
        return ret
    
    def delete_callback(self) -> None:
        return
    
    # network functions

    def get_network(self):
        return self.network_
    
    # parameters functions
    def get_parameters(self) -> List[Parameter]:
        return self.parameters_

    def set_parameters(self, parameters) -> None:
        if self.parameters_ != parameters:
            self.parameters_ = parameters
            self.parameters_changed.emit()
    
    def get_parameter(self, name) -> Optional[Parameter]:
        matches = [p for p in self.parameters_ if p.get_id()==name]
        if len(matches) > 0:
            return matches[0]
        else:
            return None
    
    @Slot()
    def connections_update(self) -> None:
        self.connections_update_impl()
    
    @Slot()
    def parameters_update(self) -> None:
        self.parameters_update_impl()

    def connections_update_impl(self) -> None:
        pass

    def parameters_update_impl(self) -> None:
        pass
    
    def default_parameters() -> List[Parameter]:
        return []

    
    name          = Property(str, get_name, notify=name_changed)
    detailed_name = Property(str, get_detailed_name, notify=detailed_name_changed)
    inports       = Property('QVariantList', get_inports, notify=inports_changed)
    outports      = Property('QVariantList', get_outports, notify=outports_changed)
    parameters    = Property('QVariantList', get_parameters, notify=parameters_changed)
    node_id       = Property(int, get_node_id, notify=node_id_changed)
    deletable     = Property(bool, get_deletable, notify=deletable_changed)

