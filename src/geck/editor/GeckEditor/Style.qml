pragma Singleton
import QtQuick 2.0
import GeckEditor 1.0

QtObject {
    id: root
    property real scale: 1.0
    // generic colors
    property color background1 : '#000018'
    property color background2 : '#262335'
    property color background3 : '#351040' // '#402340'
    property color border2: '#443864'
    property color text2: '#6060D0'
    property color pink: '#ff0091'

    property var node: NodeStyle {
        background1: root.background2
        background2: root.background3
        textColor: 'lightgray'
        borderColor: root.border2
    }
    // TODO: proper colors
    property var activeNode: NodeStyle {
        background1: '#ff0091'
        background2: root.background3
        textColor: 'lightgray'
        borderColor: root.border2
    }

    //parameter screen
    property var parameterScreen: ParameterScreenStyle {
    }

    // initial parameter screen colors
    property color initialParameterScreenBackgroundColor: '#80FFFFFF'

    // port colors
    property color portColor: text2 //'#8080FF'
    property color portBorderColor: '#404080'
    property color portTextColor: '#000000'
    property color portActivateColor: '#A0A0FF'

    // connection colors
    property color connectionColor: text2//'#8080FF'
    property color connectionSelectedColor: pink

    // Connection Kit Area Grid
    property color gridColor1: '#80303068'
    property color gridColor2: '#40303068'
}