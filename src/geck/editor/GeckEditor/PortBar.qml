import QtQuick 2.15
import QtQuick.Controls 2.15

Column {
    id: root
    spacing: 5
    property var ports: undefined
    property var isInports: false
    property var editor: undefined
    property var nodeItem: root
    Repeater {
        id: port_repeater
        model: ports
        Port {
            anchors.left: root.isInports ? parent.left : undefined
            anchors.right: root.isInports ? undefined : parent.right
            nodeObject: root
            isInport: root.isInports
            port: modelData
            editor: root.editor
        }
    }

    function get_port_by_id(port_id) {
        var port = null
        for(var i = 0; i < port_repeater.count; ++i) {
            if(port_repeater.itemAt(i).port.port_id==port_id) {
                port = port_repeater.itemAt(i)
            }
        }
        return port
    }
}