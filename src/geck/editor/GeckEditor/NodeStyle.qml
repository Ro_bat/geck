import QtQuick 2.0

QtObject {
    property color background1: '#262335'
    property color background2: '#351040'
    property color borderColor: '#443864'
    property color textColor: 'lightgray'
}