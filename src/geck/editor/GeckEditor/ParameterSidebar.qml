import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    property var node: null
    property var previousNode: null
    property bool active: false
    implicitWidth: parameterList.implicitWidth
    implicitHeight: parameterList.implicitHeight
    property var parameterScreenStyle: Style.parameterScreen
    property Component backgroundComponent: Component {
        Rectangle {
            color: parameterScreenStyle.background
        }
    }
    Loader {
        anchors.fill: parent
        sourceComponent: backgroundComponent
    }
    ParameterList {
        id: parameterList
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        parameterScreenStyle: root.parameterScreenStyle
        parameters: node ? node.node.parameters : null
    }
    Row {
        spacing: 20
        anchors.top: parameterList.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        Button {
            id: okButton
            text: "Apply"
            onClicked: root.confirm()
        }
    }

    function confirm() {
        if(active) {
            // confirm changes to parameters
            for(var i=0;i<node.node.parameters.length;++i) {
                if(node.node.parameters[i].editable) {
                    node.node.parameters[i].confirm_changes()
                }
            }
            node.node.parameters_update()
        }
    }

    onNodeChanged: {
        if(root.previousNode) {
            // reset temporary parameter values
            for(var i=0;i<root.previousNode.node.parameters.length;++i) {
                previousNode.node.parameters[i].temp_value = previousNode.node.parameters[i].value
            }
        }
        root.previousNode = root.node
    }
}
