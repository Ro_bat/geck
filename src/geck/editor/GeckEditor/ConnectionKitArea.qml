import QtQuick 2.15
import QtQuick.Controls 2.15
import GeckEditor 1.0

Item {
    id: root
    property var editor: null
    property var draggedPortItem: null
    property var node: editor.controller.network
    readonly property bool isNode: true

    Flickable {
        id: flick
        contentWidth :  (grid.width)*scaling + width*1.5
        contentHeight : (grid.height)*scaling + height*1.5
        anchors.fill: parent
        property real scaling: 1

        GraphicGrid {
            id: grid
            x: root.width*0.75
            y: root.height*0.75
            color1: Style.gridColor1
            color2: Style.gridColor2
            verticalLineStyleOffset: boundaryTop/verticalLineDistance
            horizontalLineStyleOffset: boundaryLeft/horizontalLineDistance

            property var boundaryRight: Math.ceil(4+positionHandler.xMax/horizontalLineDistance)*horizontalLineDistance
            property var boundaryBottom: Math.ceil(4+positionHandler.yMax/verticalLineDistance)*verticalLineDistance
            property var boundaryLeft: Math.ceil(-4+positionHandler.xMin/horizontalLineDistance)*horizontalLineDistance
            property var boundaryTop: Math.ceil(-4+positionHandler.yMin/verticalLineDistance)*verticalLineDistance

            width: boundaryRight - boundaryLeft
            height: boundaryBottom - boundaryTop
            transform: Scale { origin.x: 0; origin.y: 0; xScale: flick.scaling; yScale: flick.scaling}
            PositionHandler {
                 id: positionHandler
                 network: root.editor.controller.network
            }
            MouseArea {
                 anchors.fill: parent
                 onClicked: {
                     root.editor.selectedObjects = []
                     grid.focus = true
                 }
            }
            Item {
                id: nodeArea
                x: - parent.boundaryLeft
                y: - parent.boundaryTop
                property var xPrev: x
                property var yPrev: y
                onXChanged: {
                    flick.contentX += (x - xPrev)*flick.scaling
                    xPrev = x
                }
                onYChanged: {
                    flick.contentY += (y - yPrev)*flick.scaling
                    yPrev = y
                }


                Repeater {
                    id: connection_repeater
                    model: root.editor.controller.network.connections
                    Connection {
                        id: connection
                        connection: modelData
                        property var src_node_id: modelData.srcport.node_id
                        property var src_port_id: modelData.srcport.port_id
                        property var dst_node_id: modelData.dstport.node_id
                        property var dst_port_id: modelData.dstport.port_id
                        srcport: root.port_by_id(src_node_id, src_port_id)
                        dstport: root.port_by_id(dst_node_id, dst_port_id)
                        editor: root.editor
                        selected: editor.selectedObjects.includes(this)
                        onClicked: {
                            editor.selectedObjects = [connection];
                            grid.focus = true
                        }
                    }
                }
                Repeater {
                     id: node_repeater
                     model: root.editor.controller.network.nodes
                     Node {
                         id: nodeItem
                         node: modelData
                         editor: root.editor
                         property var position: positionHandler.get_position_by_id(modelData.node_id)
                         x: position.x
                         y: position.y
                         selected: editor.selectedObjects.includes(this)
                         draggedPortItem: root.draggedPortItem
                         Connections {
                            function onXChanged() {
                                position.x = x
                            }
                            function onYChanged() {
                                position.y = y
                            }
                            function onClicked(mouse) {
                               editor.selectedObjects = [nodeItem];
                               grid.focus = true
                            }
                            function onPortDragged(portItem, dragging) {
                                if(dragging) {
                                    root.editor.selectedObjects = []
                                    root.draggedPortItem = portItem
                                }
                                else {
                                    root.draggedPortItem = null
                                }
                            }
                         }
                     }
                }
                PortBar {
                    id: inport_bar
                    x: -parent.x - width/2
                    y: -parent.y + grid.height/2 - height/2
                    ports: root.editor.controller.network.network_inports
                    isInports: false
                    editor: root.editor
                }
                PortBar {
                    id: outport_bar
                    x: -parent.x + grid.width - width/2
                    y: -parent.y + grid.height/2 - height/2
                    ports: root.editor.controller.network.network_outports
                    isInports: true
                    editor: root.editor
                }
                Component.onCompleted: {
                    xPrev = x
                    yPrev = y
                }
            }
            DropArea {
                id: nodeGeneratorDropArea
                anchors.fill: parent
                keys: ['nodeGenerator']
                onDropped: (drop) => {
                    root.editor.selectedObjects = []
                    initialParameterScreen.activate(drop.source.nodeGenerator, drop.x + parent.boundaryLeft, drop.y + parent.boundaryTop)
                }
            }
            focus: true
            Keys.enabled: true
            Keys.onDeletePressed: {
                selectedObjects.forEach(element => element.delete_geck_object())
                positionHandler.cleanup()
                positionHandler.force_update_boundaries()
            }
        }
    }


    MouseArea {
        anchors.fill: parent
        property var editor: null
        acceptedButtons: Qt.NoButton
        onWheel: (wheel) => {
            var factor = 1.0
            if(wheel.angleDelta.y>0)
                factor = Math.pow(2.0,0.25)
            else if(wheel.angleDelta.y<0)
                factor = Math.pow(2.0,-0.25)
            root.zoom(factor, wheel.x, wheel.y)
            wheel.accepted = true
        }
    }

    function zoom(factor, x, y)
    {
        var origScale = flick.scaling
        var origContentX = (flick.contentX - flick.width*0.75 + x)/origScale
        var origContentY = (flick.contentY - flick.height*0.75 + y)/origScale
        flick.scaling *= factor
        flick.scaling = Math.max(1.0/16.0,flick.scaling)
        flick.scaling = Math.min(4,flick.scaling)
        flick.contentX = (origContentX)*flick.scaling + flick.width*0.75 - x
        flick.contentY = (origContentY)*flick.scaling + flick.height*0.75 - y
    }

    function node_by_id(id) {
        var node = null
        for(var i = 0; i < node_repeater.count; ++i) {
            if(node_repeater.itemAt(i).node.node_id==id) {
                node = node_repeater.itemAt(i)
            }
        }
        return node
    }

    function get_network_port(port_id) {
        var port = inport_bar.get_port_by_id(port_id)
        if(port==null) {
            port = outport_bar.get_port_by_id(port_id)
        }
        return port
    }

    function port_by_id(node_id, port_id) {
        if(node_id!=0) {
            var port = root.node_by_id(node_id).port_by_id(port_id)
            return port
        }
        else {
            var port = get_network_port(port_id)
            return get_network_port(port_id)
        }
    }

    function resetNodePositions() {
        positionHandler.reset_positions()       
    }
}