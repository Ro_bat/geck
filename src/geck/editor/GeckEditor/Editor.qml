import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import GeckEditor 1.0

Item {
    id: root
    property var selectedObjects: []
    required property var controller

    Rectangle {
        anchors.fill: parent
        color: Style.background1
    }

    Column {
        anchors.fill: parent
        ControlBar {
            id: controlBar
            width: parent.width
            onNetworkSettingsClicked: {
                root.selectedObjects = [connectionKitArea]
            }
            onResetPositionsClicked: {
                connectionKitArea.resetNodePositions()
            }
        }
        SplitView {
            width: parent.width
            height: parent.height - controlBar.height
            orientation: Qt.Horizontal
            handle: Rectangle {
            implicitWidth: 4
            implicitHeight: 4
            color: SplitHandle.pressed ? Style.background3
                : (SplitHandle.hovered ? Qt.lighter(Style.background2, 1.1) : Style.background2)
            }
            
            NodeGeneratorList {
                visible: controlBar.generatorsEnabled
                id: nodeGeneratorList
                SplitView.preferredWidth: visible ? 200 : 0
                editor: root
                z: 1
            }
            Item {
                x: 20
                ConnectionKitArea {
                    id: connectionKitArea
                    anchors.fill: parent
                    clip: true
                    editor: root
                }
                ParameterSidebar {
                    id: parameterSidebar
                    width: active ? implicitWidth : 0
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    node: (root.selectedObjects.length>0 && selectedObjects[0].isNode) ? selectedObjects[0] : null
                    active: root.selectedObjects.length>0

                    Behavior on width {
                        NumberAnimation { 
                            duration: 1000 
                            easing.type: Easing.InOutQuart;
                        }
                    }
                }
            }
        }
    }

    InitialParameterScreen {
        z: 2
        anchors.fill: parent
        id: initialParameterScreen
        connectionKitArea: connectionKitArea
        active: false
    }


}
