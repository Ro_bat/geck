import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.0

Item {
    id: root
    property var parameter: null
    property var parameterScreenStyle: Style.parameterScreenStyle
    property bool ignoreEditability: false
    readonly property bool editable: ignoreEditability || parameter.editable
    readonly property bool ok: parameter ? parameter.value_valid ? true : false : false
    readonly property var activeStyle: !editable ? parameterScreenStyle.inactive : ok ? parameterScreenStyle.active : parameterScreenStyle.error
    height: row.height
    width: row.width
    Row {
        id: row
        Text {
            id: nameText
            color: root.activeStyle.text
            text: parameter ? parameter.name + ':' : null
        }

        Rectangle {
            id: textRectangle
            color: root.activeStyle.inputField
            border.width: 1
            border.color: root.activeStyle.inputFieldAccent
            radius: 2
            width: textInput.width+10
            height: textInput.height+6
            TextInput {
                id: textInput
                readOnly: !root.editable
                x: 5
                y: 3
                width: 100
                color: root.activeStyle.inputFieldText
                selectByMouse: true
                validator: RegularExpressionValidator{regularExpression: /-?[0-9]*/}
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                onTextChanged: {
                    parameter.temp_value = parseInt(text)
                }
            }
        }
        AlertIcon {
            visible: parameter ? !parameter.value_valid : false
            ToolTip.visible: alertArea.containsMouse
            ToolTip.text: parameter ? parameter.parameter_error_text : ""
            width: textRectangle.height
            height: textRectangle.height
            MouseArea {
                id: alertArea
                hoverEnabled: true
                anchors.fill: parent
            }
        }
    }
    onParameterChanged: {
        textInput.text = parameter ? parameter.temp_value : null
    }

}
