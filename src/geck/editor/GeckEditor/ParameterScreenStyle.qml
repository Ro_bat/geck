import QtQuick 2.0

QtObject {
    property color background: '#000000'
    property var active: QtObject {
        property color text: 'white'
        property color inputField: 'white'
        property color inputFieldText: 'black'
        property color inputFieldAccent: 'lightgray'
    }
    property var inactive: QtObject {
        property color text: 'lightgray'
        property color inputField: 'lightgray'
        property color inputFieldText: 'gray'
        property color inputFieldAccent: 'gray'
    }
    property var error: QtObject {
        property color text: 'white'
        property color inputField: 'white'
        property color inputFieldText: 'red'
        property color inputFieldAccent: 'red'
    }
}
