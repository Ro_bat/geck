import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    id: root
    property var editor: null

    color: '#FF000018'
    height: column.height

    Column {
        id: column
        spacing: 5
        topPadding: 5
        bottomPadding: 5
        rightPadding: 5
        leftPadding: 5
        Repeater {
            model: editor.controller.node_generators
            NodeGeneratorListEntry {
                width: root.width - (column.leftPadding + column.rightPadding)
                height: 50
                nodeGenerator: modelData
            }
        }
    }
}