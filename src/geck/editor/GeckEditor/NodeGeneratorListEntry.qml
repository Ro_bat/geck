import QtQuick 2.15
import QtQuick.Controls 2.15
import GeckEditor 1.0

Rectangle {
    id: root
    border.color: Style.border2
    border.width: 2
    radius: 5
    color: Style.background2
    property var nodeGenerator: null
    property var dragTarget: null
    Item {
        id: innerArea
        x: root.border.width
        y: root.border.width
        width: root.width - x*2
        height: root.height - y*2
        Rectangle {
            id: imageArea
            width: Math.min(height,parent.width)
            height: parent.height
            color: 'black'
            Image {
                anchors.fill: parent
            }
        }
        Item {
            x: imageArea.width+10
            width: parent.width - x
            height: parent.height
            Text {
                id: nameText
                anchors.fill: parent
                //anchors.verticalCenter: parent.verticalCenter
                //width: Math.min(implicitWidth, parent.width)
                //height: Math.min(implicitHeight, parent.height)
                verticalAlignment: Text.AlignVCenter
                smooth: true
                clip: true
                color: Style.text2
                font.family: "Helvetica"
                font.pointSize: 24
                text: nodeGenerator.name
            }
        }
    }
    Rectangle {
        border.color: Style.border2
        border.width: 2
        radius: 5
        color: '#00000000'
        anchors.fill: parent
    }
    Rectangle {
        id: dragTargetRect
        gradient: Gradient {
            GradientStop { position: 0.0; color: Style.node.background1 }
            GradientStop { position: 1.0; color: Style.node.background2 }
        }
        border.color: Style.node.borderColor
        border.width: 2
        radius: 10
        visible: Drag.active
        width: dragText.implicitWidth + 10
        height: dragText.implicitHeight + 10
        Drag.active: dragArea.drag.active
        Drag.keys: ['nodeGenerator']
        Drag.mimeData: { "text": text, "generator" : root.nodeGenerator }
        Drag.hotSpot.x: width/2
        Drag.hotSpot.y: height/2
        property string text: root.nodeGenerator.name
        property var nodeGenerator: root.nodeGenerator
        Text {
            id: dragText
            color: Style.node.textColor
            anchors.fill: parent
            font.family: "Helvetica"
            text: parent.text
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: dragTargetRect
        drag.threshold: 0
        drag.smoothed: false
        onPositionChanged: {
            drag.target.x = mouse.x - drag.target.width/2
            drag.target.y = mouse.y - drag.target.height/2
        }
        onReleased: {
            drag.target.Drag.drop()
        }
    }
}