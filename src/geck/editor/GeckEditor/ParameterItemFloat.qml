import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    property var parameter: null
    property var parameterScreenStyle: Style.parameterScreenStyle
    property var ignoreEditability: false
    readonly property bool ok: parameter ? parameter.value_valid ? true : false : false
    height: row.height
    width: row.width
    Row {
        id: row
        Text {
            id: nameText
            text: parameter ? parameter.name + ':' : null
        }

        Rectangle {
            id: textRectangle
            color: root.ok ? root.parameterScreenStyle.inputField : root.parameterScreenStyle.inputFieldError
            border.width: 1
            border.color: root.ok ? root.parameterScreenStyle.inputFieldAccent : root.parameterScreenStyle.inputFieldAccentError
            radius: 2
            width: textInput.width+10
            height: textInput.height+6
            TextInput {
                id: textInput
                readOnly: !root.editable
                x: 5
                y: 3
                width: 100
                color: root.ok ? root.parameterScreenStyle.inputFieldText : root.parameterScreenStyle.inputFieldTextError
                selectByMouse: true
                validator: RegularExpressionValidator{regularExpression: /-?[0-9]*(.[0-9]*)?/}
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                onTextChanged: {
                    parameter.temp_value = parseInt(text)
                }
            }
        }
    }

    onParameterChanged: {
        textInput.text = parameter ? parameter.temp_value : null
    } 
}
