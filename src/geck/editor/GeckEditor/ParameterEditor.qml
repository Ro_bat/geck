import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    property var nodeGenerator: null
    property alias name: nameTextField.text
    property var parameterScreenStyle: Style.parameterScreen
    width: Math.max(grid.width,parameterList.width)
    height: grid.height + parameterList.height
    Grid {
        id: grid
        columns: 2
        spacing: 2
        Text {
            id: nameText
            text: "Name:"
        }
        TextField {
            id: nameTextField
            placeholderText: qsTr("Enter name")
            text: root.nodeGenerator ? root.nodeGenerator.name : ''
        }
    }
    ParameterList {
        id: parameterList
        y: grid.height
        parameterScreenStyle: root.parameterScreenStyle
        ignoreEditability: true
        parameters: root.nodeGenerator ? root.nodeGenerator.parameters : null
    }
}