import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    property var parameters: null
    property var parameterScreenStyle: Style.parameterScreen
    property var ignoreEditability: false
    width: implicitWidth
    height: implicitHeight
    implicitWidth: column.width
    implicitHeight: column.height
    property var parameterComponentMap: new Map([
            ['base:integer', integerComponent],
            ['base:float', floatComponent]
    ])
    Component {
        id: integerComponent
        ParameterItemInteger {
            parameterScreenStyle: root.parameterScreenStyle
        }
    }
    Component {
        id: floatComponent
        ParameterItemFloat {
            parameterScreenStyle: root.parameterScreenStyle
        }
    }
    Column {
        id: column
        Repeater {
            model: parameters
            ParameterEntry {
                id: entry
                parameterComponentMap: root.parameterComponentMap
                ignoreEditability: root.ignoreEditability
                parameter: modelData
            }
        }
    }
}
