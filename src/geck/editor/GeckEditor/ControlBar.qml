import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    id: root
    property bool generatorsEnabled: false
    width: row.width
    height: row.height+1
    color: '#00000000'
    border.width: 1
    border.color: 'black'
    signal networkSettingsClicked()
    signal resetPositionsClicked()
    Row {
        id: row
        spacing: 5
        Button {
            id: nodeGeneratorButton
            text: "Node Generators"
            onClicked: root.generatorsEnabled = !root.generatorsEnabled
        }
        Button {
            id: networkSettingsButton
            text: "Network Settings"
            onClicked: root.networkSettingsClicked()
        }
        Button {
            id: resetPositionsButton
            text: "Reset Positions"
            onClicked: root.resetPositionsClicked()
        }
    }
}
