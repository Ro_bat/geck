import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    property var nodeGenerator: null
    property var connectionKitArea: null
    property bool active: true
    property real targetX: 0
    property real targetY: 0
    property var parameterScreenStyle: Style.parameterScreen
    visible: opacity>0.0
    opacity: active ? 1.0 : 0.0


    Behavior on opacity {
        NumberAnimation { 
            duration: 500 
            easing.type: Easing.InOutQuart;
        }
    }

    width: column.width
    height: column.height

    Rectangle {
        color: parameterScreenStyle.background
        anchors.fill: parent
        MouseArea {
            anchors.fill: parent
            onWheel: {}
        }
    }

    Column {
        id: column
        anchors.centerIn: parent
        ParameterEditor {
            id: paramEditor
            nodeGenerator: root.nodeGenerator
            parameterScreenStyle: root.parameterScreenStyle
        }
        Row {
            spacing: 20
            Button {
                id: okButton
                text: "Ok"
                onClicked: root.confirm()
            }
            Button {
                id: cancelButton
                text: "Cancel"
                onClicked: root.abort()
            }
        }
    }

    function activate(nodeGenerator, x, y) {
        root.nodeGenerator = nodeGenerator
        root.targetX = x
        root.targetY = y
        root.active = true
    }

    function abort() {
        // deactivate screen
        root.active = false
        // reset default parameter values
        for(var i=0;i<root.nodeGenerator.parameters.length;++i) {
            root.nodeGenerator.parameters[i].reset_default()
        }
        root.nodeGenerator = null
    }

    function confirm() {
        if(active) {
            // confirm changes to parameters
            for(var i=0;i<root.nodeGenerator.parameters.length;++i) {
                root.nodeGenerator.parameters[i].confirm_changes()
            }
            // generate new node
            var node = root.nodeGenerator.generate(paramEditor.name)
            // place new node item
            var nodeItem = root.connectionKitArea.node_by_id(node.node_id)
            nodeItem.x = targetX - nodeItem.width/2
            nodeItem.y = targetY - nodeItem.height/2
            // deactivate screen
            root.active = false
            // reset default parameter values
            for(var i=0;i<root.nodeGenerator.parameters.length;++i) {
                root.nodeGenerator.parameters[i].reset_default()
            }
            root.nodeGenerator = null
        }
    }
}