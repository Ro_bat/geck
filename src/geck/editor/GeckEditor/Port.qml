import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import GeckEditor 1.0

import "."

Rectangle {
    id: root
    color: connectable ? dropArea.containsDrag ?  Style.portActivateColor : Style.portColor : 'grey'
    border.width: 2
    border.color: Style.portBorderColor
    radius: 10
    signal dragChanged(bool dragging)
    property var port
    property var nodeObject
    property var editor
    property var draggedPortItem: null
    readonly property bool connectable: draggedPortItem ? draggedPortItem.port.can_connect(port) || draggedPortItem==root : true

    property bool isInport
    property point inportConnectorPoint:  Qt.point(0.0       , root.height/2.0)
    property point outportConnectorPoint: Qt.point(root.width, root.height/2.0)
    property point connectorPoint: isInport ? inportConnectorPoint : outportConnectorPoint
    property alias name: text.text
    implicitWidth: Math.max(text.width + 10, height)
    implicitHeight: text.height + 5
    Text {
        id: text
        text: root.port.name
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }
    
    Item {
        id: dragTarget
        width: 3
        height: 3
        property var portQualifier: root.port.qualifier
        Drag.active: dragArea.drag.active
        Drag.hotSpot.x: 1
        Drag.hotSpot.y: 1
        Drag.keys: [(root.isInport ? 'inport' : 'outport')]
        Drag.mimeData: { "inport": 'test' }
    }
    BezierCurve {
        z: 10
        visible: dragArea.drag.active
        property real xOffset: Math.min(100, Math.hypot(b3.x-b0.x,b3.y-b0.y)/2 )
        property real yOffset: 0
        b0: root.isInport ? Qt.point(dragTarget.x,dragTarget.y) : root.connectorPoint
        b1: Qt.point(b0.x + xOffset, b0.y + yOffset)
        b2: Qt.point(b3.x - xOffset, b3.y + yOffset)
        b3: root.isInport ? root.connectorPoint : Qt.point(dragTarget.x,dragTarget.y)
        color: Style.connectionSelectedColor
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: dragTarget
        drag.threshold: 0
        drag.smoothed: false
        hoverEnabled: true
        onPositionChanged: {
            dragTarget.x = mouse.x
            dragTarget.y = mouse.y
        }
        onReleased: {
            dragTarget.Drag.drop()
        }
        Connections {
            target: dragArea.drag
            function onActiveChanged() {
                root.dragChanged(target.active)
            }
        }
    }
    DropArea {
        id: dropArea
        anchors.fill: parent

        keys: [(root.isInport ? 'outport' : 'inport')]

        onDropped: (drop) => {
            var port1 = drop.source.portQualifier
            var port2 = dragTarget.portQualifier
            var inport = root.isInport ? port1 : port2
            var outport = root.isInport ? port2 : port1
            root.editor.controller.geck_connect(inport,outport)
        }
    }
    ToolTip.visible: dragArea.containsMouse
    ToolTip.text: root.port.port_data_type.data_type_string + (root.port.port_data_type.details ? ': ' + root.port.port_data_type.details: '')
}