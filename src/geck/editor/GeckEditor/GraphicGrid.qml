import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    property real horizontalLineDistance: 50
    property real verticalLineDistance: 50
    property int horizontalBoxNum: Math.ceil(height/horizontalLineDistance)+1
    property int verticalBoxNum: Math.ceil(width/verticalLineDistance)+1
    property int verticalLineStyleOffset: 0
    property int horizontalLineStyleOffset: 0

    property color color1 : '#80FFFFFF'
    property color color2 : '#40FFFFFF'
    property int lineWidth: 3

    Repeater {
        id: horizonalLineRepeater
        property real distance: root.horizontalLineDistance
        model: root.horizontalBoxNum
        Rectangle {
            color: (Math.abs(verticalLineStyleOffset+index)%2==1) ? root.color2 : root.color1
            y: index*horizonalLineRepeater.distance
            height: root.lineWidth
            width: root.width
        }
    }

    Repeater {
        id: verticalLineRepeater
        property real distance: root.verticalLineDistance
        model: root.verticalBoxNum
        Rectangle {
            color: (Math.abs(horizontalLineStyleOffset+index)%2==1) ? root.color2 : root.color1
            x: index*verticalLineRepeater.distance
            height: root.height
            width: root.lineWidth
        }
    }
}