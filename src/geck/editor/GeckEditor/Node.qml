import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15

import "."

Rectangle {
    id: root
    gradient: Gradient {
        GradientStop { position: 0.0; color: root.nodeStyle.background1 }
        GradientStop { position: 1.0; color: root.nodeStyle.background2 }
    }
    signal clicked(QtObject mouse)
    signal portDragged(QtObject portItem, bool dragging)
    property bool selected: false
    property var nodeStyle: selected ? Style.activeNode : Style.node
    readonly property bool isNode: true
    border.color: nodeStyle.borderColor
    border.width: 2
    radius: 10

    property var node
    property var editor
    property var draggedPortItem: null
    
    function delete_geck_object() {
        if(root.node.deletable) {
            root.editor.controller.remove_node(root.node)
        }
    }

    implicitWidth: Math.max(inport_column.width + outport_column.width, name_field.width+30)
    implicitHeight: main_column.height

    Drag.active: dragArea.drag.active
    Drag.hotSpot.x: 10
    Drag.hotSpot.y: 10

    property int portSpacing: 2
    property int portBottomPadding: 5
    property int portTopPadding: 5
    
    MouseArea {
        id: dragArea
        anchors.fill: parent
    
        drag.target: parent
        drag.threshold: 0
        drag.smoothed: false
        onClicked: root.clicked(mouse)
    }

    Column {
        id: main_column
        Item {
            width: root.width
            height: name_field.height
            Text {
                id: name_field
                color: root.nodeStyle.textColor
                text: node.name
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
            }
        }
        Item {
            id: ports_area
            width: root.width
            height: Math.max(inport_column.height, outport_column.height)
            Column {
                id: inport_column
                anchors.left: parent.left
                spacing: root.portSpacing
                topPadding: root.portTopPadding
                bottomPadding: root.portBottomPadding
                Repeater {
                    id: inport_repeater
                    model: node.inports
                    Port {
                        id: inPortItem
                        anchors.left: parent.left
                        nodeObject: root
                        isInport: true
                        port: modelData
                        editor: root.editor
                        draggedPortItem: root.draggedPortItem
                        Connections {
                            function onDragChanged(dragging) {
                                root.portDragged(inPortItem, dragging)
                            }
                        }
                    }
                }
            }
            Column {
                id: outport_column
                anchors.right: parent.right
                spacing: root.portSpacing
                topPadding: root.portTopPadding
                bottomPadding: root.portBottomPadding
                Repeater {
                    id: outport_repeater
                    model: node.outports
                    Port {
                        id: outPortItem
                        anchors.right: parent.right
                        nodeObject: root
                        isInport: false
                        port: modelData
                        editor: root.editor
                        draggedPortItem: root.draggedPortItem
                        Connections {
                            function onDragChanged(dragging) {
                                root.portDragged(outPortItem, dragging)
                            }
                        }
                    }
                }
            }
        }
    }

    function port_by_id(id) {
        var node = null
        for(var i = 0; i < inport_repeater.count; ++i) {
            if(inport_repeater.itemAt(i).port.port_id==id) {
                node = inport_repeater.itemAt(i)
            }
        }
        for(var i = 0; i < outport_repeater.count; ++i) {
            if(outport_repeater.itemAt(i).port.port_id==id) {
                node = outport_repeater.itemAt(i)
            }
        }
        return node
    }
}