import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root
    property var parameter: null
    property var parameterType: parameter ? parameter.parameter_type : 0
    property var parameterComponentMap: null
    property var ignoreEditability: false
    width:  loader.width
    height: loader.height
    property var parameterValue: parameter.value

    Loader {
        id: loader
        sourceComponent: parameterComponentMap.get(parameterType)
        onLoaded: {
            item.parameter = root.parameter
            item.ignoreEditability = root.ignoreEditability
        }
    }
}
