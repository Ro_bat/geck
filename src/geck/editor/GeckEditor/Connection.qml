import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import GeckEditor 1.0

import "."

BezierCurve {
    id: root
    property var connection
    property var srcport
    property var dstport
    property var editor
    property point srcport_point: Qt.point(0,0)
    property point dstport_point: Qt.point(0,0)
    property bool selected: false
    readonly property bool isNode: false
    signal clicked(QtObject mouse)

    function delete_geck_object() {
        if(root.connection.deletable) {
            root.editor.controller.geck_disconnect(connection)
        }
    }

    property real xOffset: Math.min(100, Math.hypot(dstport_point.x-srcport_point.x,dstport_point.y-srcport_point.y)/2 )
    property real yOffset: 0

    b0: srcport_point
    b1: Qt.point(srcport_point.x + xOffset, srcport_point.y + yOffset)
    b2: Qt.point(dstport_point.x - xOffset, dstport_point.y + yOffset)
    b3: dstport_point
    color: selected ? Style.connectionSelectedColor : Style.connectionColor

    MouseArea {
        id: mouse
        anchors.fill: parent
        onPressed: if (root.is_transparent(mouse.x, mouse.y)) {
            mouse.accepted = false
        }
        onClicked: {
            if(!root.is_transparent(mouse.x, mouse.y)) {
                root.clicked(mouse)
                mouse.accepted = true
            }
            else {
                mouse.accepted = false
            }
        } 
    }

    function update_src() {
        if(srcport)
            root.srcport_point = srcport.mapToItem(srcport.nodeObject.parent, srcport.outportConnectorPoint)
    }

    function update_dst() {
        if(dstport)
            root.dstport_point = dstport.mapToItem(dstport.nodeObject.parent, dstport.inportConnectorPoint)
    }

    Connections {
        target: srcport ? srcport.nodeObject : null
        function onXChanged()     { root.update_src() }
        function onYChanged()     { root.update_src() }
        function onWidthChanged() { root.update_src() }
        function onHeightChanged(){ root.update_src() }
    }
    Connections {
        target: dstport ? dstport.nodeObject : null
        function onXChanged()     { root.update_dst() }
        function onYChanged()     { root.update_dst() }
        function onWidthChanged() { root.update_dst() }
        function onHeightChanged(){ root.update_dst() }
    }

    Connections {
        //ignoreUnknownSignals: true
        function onSrcportChanged()     { root.update_src() }
    }

    Connections {
        //ignoreUnknownSignals: true
        function onDstportChanged()     { root.update_dst() }
    }

    Component.onCompleted: {
        update_src();
        update_dst();
    }
    

}