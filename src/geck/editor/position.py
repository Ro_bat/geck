from PySide2.QtCore import QObject, Property, Signal, Slot

class Position(QObject):
    x_changed = Signal()
    y_changed = Signal()
    def __init__(self , x = 0.0, y = 0.0, parent : QObject = None):
        QObject.__init__(self, parent=parent)
        self.x_ = x
        self.y_ = y
    
    def get_x(self):
        return self.x_
    
    def set_x(self, value):
        if value != self.x_:
            self.x_ = value
            self.x_changed.emit()
    
    def get_y(self):
        return self.y_
    
    def set_y(self, value):
        if value != self.y_:
            self.y_ = value
            self.y_changed.emit()
    
    x = Property(float, get_x, set_x, notify=x_changed)
    y = Property(float, get_y, set_y, notify=y_changed)