from PySide2.QtQuick import QQuickPaintedItem
from PySide2.QtCore import QPoint, Property, Signal, Slot
from PySide2.QtGui import QColor, QPen, QPainter, QPainterPath, QImage
from PySide2.QtQml import qmlRegisterType

class BezierCurve (QQuickPaintedItem):
    def __init__(self, parent=None):
        QQuickPaintedItem.__init__(self, parent)
        self.b0_ = QPoint(0,0)
        self.b1_ = QPoint(0,0)
        self.b2_ = QPoint(0,0)
        self.b3_ = QPoint(0,0)
        self.margin_ = 50
        self.image_ = QImage()
        self.update_boundaries()
        self.color_ = QColor()
        self.update()

    def paint(self, painter):
        b0_relative = self.b0_-QPoint(self.x(),self.y())
        b1_relative = self.b1_-QPoint(self.x(),self.y())
        b2_relative = self.b2_-QPoint(self.x(),self.y())
        b3_relative = self.b3_-QPoint(self.x(),self.y())
        path = QPainterPath(b0_relative)
        #path.lineTo(p_end)
        path.cubicTo(b1_relative, b2_relative, b3_relative)
        pen = QPen(self.color_, 2)
        pen2 = QPen(QColor(0,255,255,255), 10)
        painter.setRenderHints(QPainter.Antialiasing, True)
        #painter.setPen(pen2)
        #painter.drawPath(path)
        painter.setPen(pen)
        painter.drawPath(path)
        self.image_ = QImage(self.width(), self.height(), QImage.Format_ARGB32)
        self.image_.fill(QColor(255,255,255,255))
        painter2 = QPainter(self.image_)
        painter2.setPen(pen2)
        painter2.setRenderHints(QPainter.Antialiasing, False)
        painter2.drawPath(path)

    def get_b0(self):
        return self.b0_

    def get_b1(self):
        return self.b0_

    def get_b2(self):
        return self.b0_

    def get_b3(self):
        return self.b3_

    def set_b0(self, value):
        if self.b0_ != value:
            self.b0_ = value
            self.update_boundaries()
            self.update()
            self.b0_changed.emit()

    def set_b1(self, value):
        if self.b1_ != value:
            self.b1_ = value
            self.update_boundaries()
            self.update()
            self.b1_changed.emit()

    def set_b2(self, value):
        if self.b2_ != value:
            self.b2_ = value
            self.update_boundaries()
            self.update()
            self.b2_changed.emit()

    def set_b3(self, value):
        if self.b3_ != value:
            self.b3_ = value
            self.update_boundaries()
            self.update()
            self.b3_changed.emit()

    def get_color(self):
        return self.color_
    
    def set_color(self, value):
        if self.color_ != value:
            self.color_ = value
            self.update()
            self.color_changed.emit()

    def update_boundaries(self):
        x_min = min(self.b0_.x(),self.b1_.x(),self.b2_.x(),self.b3_.x())-self.margin_
        y_min = min(self.b0_.y(),self.b1_.y(),self.b2_.y(),self.b3_.y())-self.margin_
        x_max = max(self.b0_.x(),self.b1_.x(),self.b2_.x(),self.b3_.x())+self.margin_
        y_max = max(self.b0_.y(),self.b1_.y(),self.b2_.y(),self.b3_.y())+self.margin_
        self.setX(x_min)
        self.setY(y_min)
        self.setWidth (x_max-x_min)
        self.setHeight(y_max-y_min)
    
    @Slot(int, int, result=bool)
    def is_transparent(self, x, y):
        if self.image_.valid(QPoint(x,y)):
            return (self.image_.pixelColor(x, y).red() != 0)
        else:
            return False
    
    b0_changed = Signal()
    b1_changed = Signal()
    b2_changed = Signal()
    b3_changed = Signal()
    color_changed = Signal()
    b0 = Property(QPoint, get_b0, set_b0, notify=b0_changed)
    b1 = Property(QPoint, get_b1, set_b1, notify=b1_changed)
    b2 = Property(QPoint, get_b2, set_b2, notify=b2_changed)
    b3 = Property(QPoint, get_b3, set_b3, notify=b3_changed)
    color = Property(QColor, get_color, set_color, notify=color_changed)
