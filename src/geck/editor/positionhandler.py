from PySide2.QtCore import QObject, Property, Signal, QPointF, Slot
from PySide2.QtQml import qmlRegisterType
from geck.network import Network
from typing import Any
from .position import Position

class PositionHandler(QObject):
    network_changed = Signal()
    positions_changed = Signal()
    x_min_changed = Signal()
    x_max_changed = Signal()
    y_min_changed = Signal()
    y_max_changed = Signal()
    def __init__(self):
        QObject.__init__(self)
        self.network_ = None
        self.positions_ = {}
        self.x_min_ = 0
        self.x_max_ = 0
        self.y_min_ = 0
        self.y_max_ = 0
    
    # position functions
    def get_positions(self):
        return self.positions_
    
    def set_positions(self, positions):
        self.positions_ = positions
        self.update_boundaries()
    
    @Slot(int,result=Position)
    def get_position_by_id(self, id : int):
        self.update_positions()
        if id not in self.positions_:
            self.positions_[id] = Position()
            self.positions_[id].x_changed.connect(self.update_boundaries)
        self.update_boundaries()
        return self.positions_[id]
    
    @Slot(int, int, int)
    def set_position_by_id(self, id : int, x : float, y: float):
        self.positions_[id].x = x
        self.positions_[id].y = y
        self.update_boundaries()
    
    # network functions
    def get_network(self):
        return self.network_
    
    def set_network(self, network):
        self.network_ = network
        self.network_changed.emit()

    def get_x_min(self):
        return self.x_min_

    def get_x_max(self):
        return self.x_max_

    def get_y_min(self):
        return self.y_min_

    def get_y_max(self):
        return self.y_max_
    
    def update_boundaries(self):
        if len(self.positions) > 0:
            x_values = {self.positions_[p].x for p in self.positions_}
            y_values = {self.positions_[p].y for p in self.positions_}
            self.x_min_ = min(x_values)
            self.x_max_ = max(x_values)
            self.y_min_ = min(y_values)
            self.y_max_ = max(y_values)
        else:
            self.x_min_ = 0
            self.x_max_ = 0
            self.y_min_ = 0
            self.y_max_ = 0
        self.x_min_changed.emit()
        self.x_max_changed.emit()
        self.y_min_changed.emit()
        self.y_max_changed.emit()
    
    @Slot()
    def force_update_boundaries(self):
        self.update_boundaries()

    def find_directly_connected_nodes(self, node_id, connections):
        result = {c.get_dstport().get_node_id() for c in connections if c.get_srcport().get_node_id()==node_id}
        result.update({c.get_srcport().get_node_id() for c in connections if c.get_dstport().get_node_id()==node_id})
        result = {id for id in result if id != None}
        return result
    
    def find_all_connected_nodes(self, node_id, connections):
        connected_nodes = self.find_directly_connected_nodes(node_id, connections)
        result = {node_id}
        result.update(connected_nodes)
        while True:
            new_nodes = set()
            for node in connected_nodes:
                new_nodes.update(self.find_directly_connected_nodes(node,connections))
            count_before = len(result)
            result.update(new_nodes)
            count_after = len(result)
            connected_nodes = new_nodes
            if count_before==count_after:
                break
        return result

    def find_subgraphs(self, nodes, connections):
        used_nodes = set()
        subgraphs = []
        node_ids = [node.get_node_id() for node in nodes]
        for node_id in node_ids:
            if node_id in used_nodes:
                continue
            connected = self.find_all_connected_nodes(node_id, connections)
            used_nodes.update(connected)
            subgraphs.append(connected)
        return subgraphs
    
    def get_nodes_without_inputs(self, node_ids, connections):
        nodes = set()
        for node_id in node_ids:
            inbound_connections = [c for c in connections if (c.get_dstport().get_node_id() == node_id) and (c.get_srcport().get_node_id() != None)]
            if len(inbound_connections)==0:
                nodes.add(node_id)
        return nodes

    def get_missing_inputs(self, node_id, node_layers, connections):
        inputs = {c.get_srcport().get_node_id() for c in connections if c.get_dstport().get_node_id()==node_id}
        inputs = {node for node in inputs if node not in node_layers}
        return inputs

    def get_existing_inputs(self, node_id, node_layers, connections):
        inputs = {c.get_srcport().get_node_id() for c in connections if c.get_dstport().get_node_id()==node_id}
        inputs = {node for node in inputs if node in node_layers}
        return inputs
    
    def select_new_node(self, node_layers, unused_nodes, connections):
        # fallback function, used for graphs with cycles
        
        # find nodes with minimal number of missing inputs
        missing_input_dict = {node : len(self.get_missing_inputs(node, node_layers, connections)) for node in unused_nodes}
        min_missing_input_num = min(missing_input_dict.values())
        potential_nodes = {node for node in unused_nodes if missing_input_dict[node]==min_missing_input_num}
        # find potential nodes with minimal layer
        max_layer_dict = {}
        for node in potential_nodes:
            try:
                # this might fail, as the set of existing inputs can be empty
                source_count = max({node_layers[source] for source in self.get_existing_inputs(node, node_layers, connections)})
            except:
                source_count = 0
            max_layer_dict[node] = source_count
        #max_layer_dict = {node : max({node_layers[source] for source in self.get_existing_inputs(node, node_layers, connections)}) for node in potential_nodes}
        min_layer = min(max_layer_dict.values())
        selected_nodes = {node for node in potential_nodes if max_layer_dict[node]==min_layer}

        result = list(selected_nodes)[0]
        return result, min_layer+1


    def get_layers(self, node_ids, connections):
        # get level 0 nodes (without input)
        node_layers = {node : 0 for node in self.get_nodes_without_inputs(node_ids, connections)}
        # if no level 0 nodes exist the first node is selected (TODO: this is a suboptimal strategy)
        if len(node_layers) == 0:
            node_layers[node_ids[0]] = 0
        # determine maximum depth and unused nodes
        maximum_depth = len(node_ids) - len(node_layers)
        unused_nodes = set(node_ids) - set(node_layers)
        # build layers iteratively
        while len(unused_nodes)>0:
            for i in range(maximum_depth):
                if len(unused_nodes)==0:
                    break
                new_nodes = set()
                for node in unused_nodes:
                    source_nodes = self.get_missing_inputs(node, node_layers, connections)
                    if len(source_nodes)==0:
                        new_nodes.add(node)
                for new_node in new_nodes:
                    new_layer = max({node_layers[node] for node in self.get_existing_inputs(new_node, node_layers, connections)}) + 1
                    node_layers[new_node] = new_layer
                unused_nodes = unused_nodes - new_nodes
            if len(unused_nodes)>0:
                selected_node, layer = self.select_new_node(node_layers, unused_nodes, connections)
                node_layers[selected_node] = layer
                unused_nodes.remove(selected_node)
        return node_layers
    
    def produce_positions(self, node_layers, offset=(0,0), overwrite_existing_position=False):
        layers = [node_layers[n] for n in node_layers]
        layer_ctr = {}
        for layer in layers:
            layer_ctr[layer]=0
        for node in node_layers:
            layer = node_layers[node]
            if node not in self.positions_:
                self.positions_[node] = Position(layer*300 + offset[0], layer_ctr[layer]*150 + offset[1])
                #self.positions_[node].connect(self.update_boundaries)
                self.positions_[node].x_changed.connect(self.update_boundaries)
                layer_ctr[layer] += 1
            elif overwrite_existing_position:
                self.positions_[node].set_x(layer*300 + offset[0])
                self.positions_[node].set_y(layer_ctr[layer]*150 + offset[1])
                layer_ctr[layer] += 1
        return (len(layers)*300, max(layer_ctr)*150)

    def update_positions(self, overwrite_existing_position=False):
        if self.network_ == None:
            return
        # remove positions without corresponding nodes
        node_ids = {n.get_node_id() for n in self.network_.get_nodes()}
        self.positions_ = {p : self.positions_[p] for p in self.positions_ if p in node_ids}
        # find subgraphs (as sets of nodes)
        subgraphs = self.find_subgraphs(self.network_.get_nodes(), self.network_.get_connections())
        single_entry_subgraphs = [subgraph for subgraph in subgraphs if len(subgraph)==1]
        multiple_entry_subgraphs = [subgraph for subgraph in subgraphs if len(subgraph)>1]
        offset = (0,0)
        for subgraph in multiple_entry_subgraphs:
            subgraph = [node for node in subgraph]
            layers = self.get_layers(subgraph, self.network_.get_connections())
            size = self.produce_positions(layers, offset, overwrite_existing_position=overwrite_existing_position)
            offset = (0, offset[1] + size[1] + 200)
        for subgraph in single_entry_subgraphs:
            subgraph = [node for node in subgraph]
            layers = self.get_layers(subgraph, self.network_.get_connections())
            size = self.produce_positions(layers, offset, overwrite_existing_position=overwrite_existing_position)
            offset = (0, offset[1] + size[1] + 200)
    
    @Slot()
    def reset_positions(self):
        self.update_positions(overwrite_existing_position = True)
    
    @Slot()
    def cleanup(self) -> None:
        node_ids = {n.get_node_id() for n in self.network_.get_nodes()}
        self.positions_ = {p : self.positions_[p] for p in self.positions_ if p in node_ids}
    
    network  = Property(Network, get_network, set_network, notify=network_changed)
    positions = Property('QVariantList', get_positions, set_positions, notify=positions_changed)
    xMin = Property(float, get_x_min, notify=x_min_changed)
    xMax = Property(float, get_x_max, notify=x_max_changed)
    yMin = Property(float, get_y_min, notify=y_min_changed)
    yMax = Property(float, get_y_max, notify=y_max_changed)