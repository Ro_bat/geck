from .curve import BezierCurve
from .positionhandler import PositionHandler
from .position import Position
from PySide2.QtQml import qmlRegisterType
import os

def register_geckeditor(engine):
    import_path = os.path.dirname(__file__)
    engine.addImportPath(import_path)
    qmlRegisterType(PositionHandler, 'GeckEditor', 1, 0, 'PositionHandler')
    qmlRegisterType(BezierCurve, 'GeckEditor', 1, 0, 'BezierCurve')
    qmlRegisterType(Position, 'GeckEditor', 1, 0, 'Position')