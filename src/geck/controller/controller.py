from geck.network import PortQualifier, Port, Connection, Network, NodeGenerator
from PySide2.QtCore import Slot, Property, Signal, QObject
from enum import Enum
from typing import List

# for testing
from geck.network import Node

class Direction(Enum):
    UpDown = 0
    LeftRight = 1
    DownUp = 2
    RightLeft = 3

class Controller(QObject):
    network_changed = Signal()
    node_generators_changed = Signal()

    def __init__(self, direction = Direction.LeftRight, network : Network = Network('Network',detailed_name='Network') , parent : QObject = None) -> None:
        QObject.__init__(self, parent = parent)
        self.network_ = network
        self.node_generators_ = []
        self.direction = direction

    def connect_ports_internal(self, port1 : PortQualifier, port2 : PortQualifier, controller_data=None) -> None:
        # check if this is a valid connection here
        self.network_.geck_connect(Connection(port1, port2, controller_data=controller_data))

    def disconnect_ports_internal(self, port1 : PortQualifier, port2 : PortQualifier, controller_data=None) -> None:
        # check if this it's okay to disconnect this
        self.network_.geck_disconnect(Connection(port1, port2, controller_data=controller_data))

    # connection management
    @Slot(PortQualifier, PortQualifier)
    def geck_connect(self, port1 : PortQualifier, port2 : PortQualifier, controller_data : bool = None) -> None:
        self.connect_ports_internal(port1, port2, controller_data)

    @Slot(Connection)
    def geck_disconnect(self, connection : Connection) -> None:
        self.network_.geck_disconnect(connection)
    
    # node removal

    @Slot(Node)
    def remove_node(self, node : Node) -> None:
        self.network_.remove_node(node)
        return

    #
    def get_network(self) -> Network:
        return self.network_
    
    # Node Generator functions
    def add_node_generator(self, node_generator):
        self.node_generators_.append(node_generator)
        node_generator.set_controller(self)
        self.node_generators_changed.emit()
    
    def get_node_generators(self) -> List[NodeGenerator]:
        return self.node_generators_
    
    def set_node_generators(self, node_generators : List[NodeGenerator]):
        self.node_generators_ = node_generators
        self.node_generators_changed.emit()

    network         = Property(Network, get_network, notify=network_changed)
    node_generators = Property('QVariantList', get_node_generators, set_node_generators, notify=node_generators_changed)
