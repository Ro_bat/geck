from geck.network import PortDataType
from copy import deepcopy

class AudioType(PortDataType):
    def generic_type_check(port_data_a, port_data_b):
        data_type_eq = PortDataType.data_type_equal(port_data_a, port_data_b)
        return data_type_eq

    def __init__(self):
        PortDataType.__init__(self,
                              data_type='jack:Audio',
                              data_type_string="Audio",
                              connection_check_function=AudioType.generic_type_check)

    # copy
    def __deepcopy__(self, memo):
        copy = type(self)()
        return copy