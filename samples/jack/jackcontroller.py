from geck.controller import Controller
from geck.network import Node, Connection, PortQualifier, Port
import jack
from PySide2.QtCore import Slot, Signal, QObject
from datatypes import AudioType

class JackController(Controller):
    jack_port_registration_signal = Signal(jack.Port, bool)
    jack_port_connect_signal = Signal(jack.Port, jack.Port, bool)
    jack_port_rename_signal = Signal(jack.Port, str, str)
    def __init__(self) -> None:
        Controller.__init__(self)
        self.jack_client = jack.Client('geck_jack_controller', servername=None)
        self.update_jack_properties()
        #self.jack_client.set_client_registration_callback(self.client_registration_callback)

        self.jack_port_registration_signal.connect(self.port_registration_callback_slot)
        self.jack_client.set_port_registration_callback(self.port_registration_jack_callback)
        
        self.jack_port_connect_signal.connect(self.port_connect_slot)
        self.jack_client.set_port_connect_callback(self.port_connect_jack_callback)
        
        # set_port_rename_callback does not work for JACK1
        try:
            self.jack_client.set_port_rename_callback(self.port_rename_jack_callback)
            self.jack_port_rename_signal.connect(self.port_rename_slot)
        except AttributeError:
            pass
        self.jack_client.activate()

    # port registration functions
    def port_registration_jack_callback(self, port: jack.Port, register: bool) -> None:
        self.jack_port_registration_signal.emit(port, register)
    
    @Slot(jack.Port, bool)
    def port_registration_callback_slot(self, port: jack.Port, register: bool) -> None:
        self.port_registration_callback(port, register)

    def port_registration_callback(self, port: jack.Port, register: bool) -> None:
        self.update_jack_nodes()

    # port connect functions
    def port_connect_jack_callback(self, port1 : jack.Port, port2 : jack.Port, connect: bool) -> None:
        self.jack_port_connect_signal.emit(port1, port2, connect)
    
    @Slot(jack.Port, jack.Port, bool)
    def port_connect_slot(self, port1 : jack.Port, port2 : jack.Port, connect: bool) -> None:
        self.port_connect_callback(port1, port2, connect)
    
    def port_connect_callback(self, port1 : jack.Port, port2 : jack.Port, connect: bool) -> None:
        self.update_jack_properties()
        return
    
    # port rename functions
    def port_rename_jack_callback(self, port: jack.Port, old_name: str, new_name: str) -> None:
        self.jack_port_rename_signal.emit(port, old_name, new_name)

    @Slot(jack.Port, str, str)
    def port_rename_slot(self, port: jack.Port, old_name: str, new_name: str) -> None:
        self.port_rename_callback(port, old_name, new_name)

    def port_rename_callback(self, port: jack.Port, old_name: str, new_name: str) -> None:
        client_name, port_name = old_name.split(':')
        new_client_name, new_port_name = new_name.split(':')
        geck_port = self.get_network().get_node_by_node_name(client_name).get_port_by_name(port_name)
        if geck_port:
            geck_port.set_name(new_port_name)
    
    # geck_connect/disconnect overrides
    @Slot(Connection)
    def geck_disconnect(self, connection : Connection) -> None:
        src_port = self.get_network().get_port_by_qualifier(connection.get_srcport())
        dst_port = self.get_network().get_port_by_qualifier(connection.get_dstport())
        self.jack_client.disconnect(src_port.get_controller_data(), dst_port.get_controller_data())
        self.get_network().geck_disconnect(connection)

    @Slot(PortQualifier, PortQualifier)
    def geck_connect(self, port1 : PortQualifier, port2 : PortQualifier, controller_data : bool = None) -> None:
        src_port = self.network_.get_port_by_qualifier(port1)
        dst_port = self.network_.get_port_by_qualifier(port2)
        connection = Connection(port1, port2, controller_data=controller_data)
        if not self.get_network().has_connection(connection):
            self.jack_client.connect(src_port.get_controller_data(), dst_port.get_controller_data())
            self.get_network().geck_connect(connection)
    
    def get_geck_port(self, jack_port) -> Port:
        try:
            name_split = jack_port.name.split(':')
            node_name = ':'.join(name_split[0:-1])
            port_name = name_split[-1]
            return self.get_network().get_node_by_node_name(node_name).get_port_by_name(port_name)
        except:
            return None
    
    def update_jack_nodes(self) -> None:
        # create new nodes/ports
        ports = self.jack_client.get_ports()
        for port in ports:
            name_split = port.name.split(':')
            node_name = ':'.join(name_split[0:-1])
            port_name = name_split[-1]
            node = self.get_network().get_node_by_node_name(node_name)
            if node == None:
                node = Node(self.get_network(), node_name, None)
                self.get_network().add_node(node)
            geck_port = node.get_port_by_name(port_name)
            if geck_port == None:
                if port.is_input:
                    node.add_inport(port_name, controller_data=port).set_port_data_type(AudioType())
                elif port.is_output:
                    node.add_outport(port_name, controller_data=port).set_port_data_type(AudioType())
        # remove ports not in the list provided by JACK
        full_port_names = {p.name for p in ports}
        for node in self.get_network().get_nodes():
            node_name = node.get_name()
            for port in node.get_ports():
                port_name = port.get_name()
                full_port_name = node_name + ':' + port_name
                if full_port_name not in full_port_names:
                    node.remove_port(port.get_port_id())
        # delete empty nodes
        for node in self.get_network().get_nodes():
            if len(node.get_ports())==0:
                self.get_network().remove_node(node)
        
    def update_jack_connections(self) -> None:
        for port in self.jack_client.get_ports():
            geck_outport = self.get_geck_port(port)
            if geck_outport == None:
                self.update_jack_nodes()
                geck_outport = self.get_geck_port(port)
            outport_qualifier = geck_outport.get_qualifier()
            if port.is_output:
                for connection in self.jack_client.get_all_connections(port):
                    geck_inport = self.get_geck_port(connection)
                    if geck_inport == None:
                        self.update_jack_nodes()
                        geck_inport = self.get_geck_port(connection)
                    inport_qualifier = geck_inport.get_qualifier()
                    geck_connection = Connection(outport_qualifier, inport_qualifier)
                    if geck_connection not in self.get_network().get_connections():
                        self.get_network().geck_connect(geck_connection)

    def update_jack_properties(self) -> None:
        self.update_jack_nodes()
        self.update_jack_connections()
        return
        # add connections
        for port in self.jack_client.get_ports():
            if port.is_output:
                for connection in self.jack_client.get_all_connections(port):
                    self.port_connect_callback(port, connection, True)
        