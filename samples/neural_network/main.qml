import QtQuick 2.15
import QtQuick.Controls 2.15
import GeckEditor 1.0

Rectangle {
    id: root
    color: 'white'
    property var controller
    Editor {
        controller: root.controller
        anchors.fill: parent
    }
}
