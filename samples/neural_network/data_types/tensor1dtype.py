from geck.network import PortDataType
from copy import deepcopy

class Tensor1DType(PortDataType):
    def tensor_type_check(port_data_a, port_data_b):
        data_type_eq = PortDataType.data_type_equal(port_data_a, port_data_b)
        either_detail_none = (port_data_a.get_details()==[]) or (port_data_b.get_details()==[])
        return data_type_eq and either_detail_none

    def __init__(self):
        PortDataType.__init__(self,
                              data_type='neural-network:Tensor1d',
                              data_type_string="1D Tensor",
                              connection_check_function=Tensor1DType.tensor_type_check)
        self.size_ = None
        self.update_details_string()
        pass

    def set_size(self, size) -> None:
        if size != self.size_:
            self.size_ = size
            self.update_details_string()

    def get_size(self):
        return self.size_

    def update_details_string(self):
        if self.size_  == None:
            self.set_details('Size: Empty')
        else:
            self.set_details('Size: {}'.format(self.size_))

    # copy
    def __deepcopy__(self, memo):
        copy = type(self)()
        copy.set_size(deepcopy(self.get_size()))
        return copy