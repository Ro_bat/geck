from geck.network import PortDataType
from copy import deepcopy

class Tensor3DType(PortDataType):
    def tensor_type_check(port_data_a, port_data_b):
        data_type_eq = PortDataType.data_type_equal(port_data_a, port_data_b)
        either_detail_none = (port_data_a.get_details()==[]) or (port_data_b.get_details()==[])
        return data_type_eq and either_detail_none

    def __init__(self):
        PortDataType.__init__(self,
                              data_type='neural-network:Tensor3d',
                              data_type_string="3D Tensor",
                              connection_check_function=Tensor3DType.tensor_type_check)
        self.dimensions_ = [None, None, None]
        self.update_details_string()
        pass

    def set_dimensions(self, dimensions) -> None:
        if dimensions != self.dimensions_:
            self.dimensions_ = dimensions
            self.update_details_string()

    def get_dimensions(self):
        return self.dimensions_

    def update_details_string(self):
        if self.dimensions_  == []:
            self.set_details('Empty')
        else:
            self.set_details(' ✕ '.join(str(x) for x in self.dimensions_))

    # copy
    def __deepcopy__(self, memo):
        copy = type(self)()
        copy.set_dimensions(deepcopy(self.get_dimensions()))
        return copy