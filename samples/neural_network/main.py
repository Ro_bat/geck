import os, sys, json, urllib.request
import PySide2.QtQml
from PySide2.QtQuick import QQuickView
from PySide2.QtCore import QStringListModel, Qt, QUrl
from PySide2.QtGui import QGuiApplication

from geck.controller import Controller
from geck.network import Node, Connection, NodeGenerator
from geck.editor import register_geckeditor
from geck.network.parameters import IntegerParameter

from nodes import ConvolutionNode, MaxPoolNode, ReLUNode, FullyConnectedNode, FlattenNode, NeuralNetwork
from data_types import Tensor3DType

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL) # this allows for the program to quit with ctrl+c

def register_node_generators(controller : Controller) -> None:
    controller.add_node_generator(NodeGenerator(ConvolutionNode))
    controller.add_node_generator(NodeGenerator(MaxPoolNode))
    controller.add_node_generator(NodeGenerator(ReLUNode))
    controller.add_node_generator(NodeGenerator(FullyConnectedNode))
    controller.add_node_generator(NodeGenerator(FlattenNode))

def main():
    # initialize controller
    control = Controller(network=NeuralNetwork(parameters=NeuralNetwork.default_parameters()))
    register_node_generators(control)

    #Set up the application window
    app = QGuiApplication(sys.argv)


    view = QQuickView()
    register_geckeditor(view.engine())
    
    # set controller
    view.setInitialProperties({'controller': control})

    # load qml
    view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__),"main.qml")))

    view.resize(600,600)
    view.setResizeMode(QQuickView.SizeRootObjectToView)

    #Show the window
    if view.status() == QQuickView.Error:
        sys.exit(-1)
    view.show()

    #execute and cleanup
    app.exec_()
    del view

if __name__=='__main__':
    main()
