from geck.network import Node, Parameter, PortDataType
from typing import List
from copy import deepcopy
from data_types import Tensor3DType, Tensor1DType
from geck.network.parameters import IntegerParameter

class FlattenNode(Node):
    def __init__(self, network, name, parameters : List[Parameter] = [], parent = None) -> None:
        Node.__init__(self,
                      network,
                      name,
                      parameters=parameters, 
                      detailed_name='Flatten',
                      deletable=True,
                      parent = parent)
        self.inport_ = self.add_inport('in')

        self.inport_.set_port_data_type(Tensor3DType())
        self.outport_ = self.add_outport('out')
        self.outport_.set_port_data_type(Tensor1DType())
        # TODO: validate parameters

    def node_name():
        return 'Flatten'

    def update_output(self):
        inport = self.inport_
        incoming_connections = inport.get_connections()
        if len(incoming_connections) > 0:
            source_port_qualifier = incoming_connections[0].get_srcport()
            source_port = self.get_network().get_port_by_qualifier(source_port_qualifier)
            data_type = source_port.get_port_data_type()
            self.inport_.set_port_data_type(deepcopy(data_type))
    
            output_data = Tensor1DType()
            dimensions = data_type.get_dimensions()
            output_data.set_size(int(dimensions[0]*dimensions[1]*dimensions[2]))
            self.outport_.set_port_data_type(output_data)
            pass
        else:
            self.inport_.set_port_data_type(Tensor3DType())
            self.outport_.set_port_data_type(Tensor1DType())
            pass
        pass

    def connections_update_impl(self) -> None:
        self.update_output()

    def parameters_update_impl(self) -> None:
        self.update_output()
    
    def default_parameters() -> List[Parameter]:
        return []
