from geck.network import Node, Parameter, PortDataType
from typing import List
from copy import deepcopy
from data_types import Tensor3DType
from geck.network.parameters import IntegerParameter

class ConvolutionNode(Node):
    def __init__(self, network, name, parameters : List[Parameter] = [], parent = None) -> None:
        Node.__init__(self,
                      network,
                      name,
                      parameters=parameters, 
                      detailed_name='2D Convolution',
                      deletable=True,
                      parent = parent)
        self.inport_ = self.add_inport('in')

        self.inport_.set_port_data_type(Tensor3DType())
        self.outport_ = self.add_outport('out')
        self.outport_.set_port_data_type(Tensor3DType())
        # TODO: validate parameters

    def node_name():
        return 'Convolution'

    def update_output(self):
        kernel_size_x = self.get_parameter('size_x').get_value()
        kernel_size_y = self.get_parameter('size_y').get_value()
        filters       = self.get_parameter('filters').get_value()
        stride_x      = self.get_parameter('stride_x').get_value()
        stride_y      = self.get_parameter('stride_y').get_value()
        inport = self.inport_
        incoming_connections = inport.get_connections()
        if len(incoming_connections) > 0:
            source_port_qualifier = incoming_connections[0].get_srcport()
            source_port = self.get_network().get_port_by_qualifier(source_port_qualifier)
            data_type = source_port.get_port_data_type()
            self.inport_.set_port_data_type(deepcopy(data_type))
            output_data = deepcopy(data_type)
            dimensions = data_type.get_dimensions()
            output_data.set_dimensions([int((dimensions[0]-kernel_size_x+1)/stride_x), int((dimensions[1]-kernel_size_y+1)/stride_y), filters])
            self.outport_.set_port_data_type(output_data)
            pass
        else:
            self.inport_.set_port_data_type(Tensor3DType())
            self.outport_.set_port_data_type(Tensor3DType())
            pass
        pass

    def connections_update_impl(self) -> None:
        self.update_output()

    def parameters_update_impl(self) -> None:
        self.update_output()
    
    def default_parameters() -> List[Parameter]:
        return [IntegerParameter('Size (X)','size_x',default_value=3, min_value=1),
                IntegerParameter('Size (Y)','size_y',default_value=3, min_value=1),
                IntegerParameter('Filters','filters',default_value=1, min_value=1),
                IntegerParameter('Stride (X)','stride_x',default_value=1, min_value=1),
                IntegerParameter('Stride (Y)','stride_y',default_value=1, min_value=1)]
