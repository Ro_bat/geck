from geck.network import Network, Parameter, PortDataType
from typing import List
from data_types import Tensor3DType
from copy import deepcopy
from geck.network.parameters import IntegerParameter

class NeuralNetwork(Network):
    def __init__(self, name :str ='Neural Network', parameters=[], detailed_name : str=None, parent = None) -> None:
        Network.__init__(self,name=name,parameters=parameters,detailed_name=detailed_name,parent=parent)
        self.inport_  = self.add_inport('in')
        inport_type = Tensor3DType()
        self.inport_.set_port_data_type(inport_type)
        self.outport_ = self.add_outport('out')
        self.update_output()
    
    def connections_update_impl(self) -> None:
        self.update_output()

    def parameters_update_impl(self) -> None:
        self.update_output()
    
    def update_output(self):
        size_x   = self.get_parameter('size_x').get_value()
        size_y   = self.get_parameter('size_y').get_value()
        channels = self.get_parameter('channels').get_value()
        self.inport_.get_port_data_type().set_dimensions([size_x,size_y,channels])
        outport = self.outport_
        outgoing_connections = outport.get_connections()
        if len(outgoing_connections) > 0:
            source_port_qualifier = outgoing_connections[0].get_srcport()
            source_port = self.get_port_by_qualifier(source_port_qualifier)
            data_type = source_port.get_port_data_type()
            outport.set_port_data_type(deepcopy(data_type))
            pass
        else:
            self.outport_.set_port_data_type(PortDataType())
            pass
        pass
    
    def default_parameters() -> List[Parameter]:
        return [IntegerParameter('Input Size (X)','size_x',default_value=100, min_value=1),
                IntegerParameter('Input Size (Y)','size_y',default_value=100, min_value=1),
                IntegerParameter('Input Channels','channels',default_value=3, min_value=1)]