from geck.network import Node, Parameter, PortDataType
from typing import List
from copy import deepcopy
from geck.network.parameters import IntegerParameter
from datatypes import GenericType

class GenericNode(Node):
    def __init__(self, network, name, parameters : List[Parameter] = [], parent = None) -> None:
        Node.__init__(self,
                      network,
                      name,
                      parameters=parameters, 
                      detailed_name='Generic Node',
                      deletable=True,
                      parent = parent)
        for i in range(self.get_parameter('inports').get_value()):
            new_node = self.add_inport('in_{}'.format(i))
            new_node.set_port_data_type(GenericType())
        for i in range(self.get_parameter('outports').get_value()):
            new_node = self.add_outport('out_{}'.format(i))
            new_node.set_port_data_type(GenericType())

    def node_name():
        return 'Generic Node'
    
    def default_parameters() -> List[Parameter]:
        return [IntegerParameter('Inports','inports',default_value=1, min_value=0),
                IntegerParameter('Outports','outports',default_value=1, min_value=0)]
