import os, sys, json, urllib.request
import PySide2.QtQml
from PySide2.QtQuick import QQuickView
from PySide2.QtCore import QStringListModel, Qt, QUrl
from PySide2.QtGui import QGuiApplication

from geck.controller import Controller
from geck.network import Connection, NodeGenerator
from geck.editor import register_geckeditor
from geck.network.parameters import IntegerParameter
from nodes import GenericNode
from datatypes import GenericType

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL) # this allows for the program to quit with ctrl+c


def establish_test_network(controller : Controller) -> None:
    node1 = GenericNode(controller.get_network(), 'Generic Node 1', parameters=GenericNode.default_parameters())
    node2 = GenericNode(controller.get_network(), 'Generic Node 2', parameters=GenericNode.default_parameters())
    node3 = GenericNode(controller.get_network(), 'Generic Node 3', parameters=GenericNode.default_parameters())
    node4 = GenericNode(controller.get_network(), 'Generic Node 4', parameters=GenericNode.default_parameters())
    node5 = GenericNode(controller.get_network(), 'Generic Node 5', parameters=GenericNode.default_parameters())
    controller.network.add_node(node1)
    controller.network.add_node(node2)
    controller.network.add_node(node3)
    controller.network.add_node(node4)
    controller.network.add_node(node5)
    controller.geck_connect( node2.get_outports()[0].get_qualifier(), node1.get_inports()[0].get_qualifier() )
    controller.geck_connect( node3.get_outports()[0].get_qualifier(), node1.get_inports()[0].get_qualifier() )
    controller.geck_connect( node3.get_outports()[0].get_qualifier(), node1.get_inports()[0].get_qualifier() )
    controller.geck_connect( node3.get_outports()[0].get_qualifier(), node4.get_inports()[0].get_qualifier() )
    controller.geck_connect( node4.get_outports()[0].get_qualifier(), node5.get_inports()[0].get_qualifier() )
    controller.geck_connect( node1.get_outports()[0].get_qualifier(), node5.get_inports()[0].get_qualifier() )
    controller.geck_connect( node5.get_outports()[0].get_qualifier(), node1.get_inports()[0].get_qualifier() )
    controller.network.add_inport('in').set_port_data_type(GenericType())
    controller.network.add_outport('out').set_port_data_type(GenericType())
    controller.geck_connect(controller.get_network().get_inports()[0].get_qualifier(), node2.get_inports()[0].get_qualifier())
    controller.geck_connect(node5.get_outports()[0].get_qualifier(), controller.get_network().get_outports()[0].get_qualifier())

def register_node_generators(controller : Controller) -> None:
    controller.add_node_generator(NodeGenerator(GenericNode))
    pass

def main():
    # initialize controller
    control = Controller()
    establish_test_network(control)
    register_node_generators(control)

    #Set up the application window
    app = QGuiApplication(sys.argv)


    view = QQuickView()
    register_geckeditor(view.engine())
    
    # set controller
    view.setInitialProperties({'controller': control})

    # load qml
    view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__),"main.qml")))

    view.resize(600,600)
    view.setResizeMode(QQuickView.SizeRootObjectToView)

    #Show the window
    if view.status() == QQuickView.Error:
        sys.exit(-1)
    view.show()

    #execute and cleanup
    app.exec_()
    del view

if __name__=='__main__':
    main()
