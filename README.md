# GECK

**This project is deprecated. Check [nodular](https://gitlab.com/Ro_bat/nodular) instead.**

GECK (Generic Extensible Connection Kit) aims at providing a framework and graphical editor for systems that can be described as a network of nodes with in- and outputs and connections between them.

Planned use-cases for this are:
* JACK / Pipewire connection editor
* Neural Network Editor
* Other cool stuff probably

It is currently in a very early stage of development.

## Samples

The current samples can be run with the following commands:
* Generic Node Editor: ./samples/generic/run.sh
* JACK: ./samples/jack/run.sh
